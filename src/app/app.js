
import React from 'react';
import { render } from 'react-dom';

// import { CustomApp } from './containers/Array';
import { CustomApp } from './containers/Fetch';

render(<CustomApp/> , document.querySelector( '#leApp' ) );