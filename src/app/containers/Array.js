
import React from 'react';

import '../favicon.ico';
import '../css/boots.scss';
import '../js/index.js';

class CustomApp extends React.Component {
  constructor() {
    super();
    this.state = {
      stooges: [
        {name: "Moe" , id: 1, weakness: "falling prey to a heavy tool to the head" }
        , {name: "Larry" , id: 2, weakness: "suddenly ripping out a handful of hair" }
        , {name: "Curly" , id: 3, weakness: "receiving double poke to the eyes" }
        , {name: "Shemp" , id: 4, weakness: "a pair of pliers to the nose" }
        , {name: "Joe" , id: 5, weakness: "receiving a kick to the arse" }
        , {name: "Curly Joe" , id: 6, weakness: "getting smashed with a pie to the face" }
      ]
    };
  }
  render(){
    const {stooges} = this.state;  // const stooges = this.state.stooges;
    const list = stooges.map( (el) => { return (<li key={el.id}>{el.name}&rsquo;s achilles heel is {el.weakness}</li>) } );
    
    return(
      <div className="container">
        <div className="row">
          <h2>Stooges</h2>
          <ul>
            {list}
          </ul>
        </div>
      </div>
    );
  };
}

export {CustomApp};