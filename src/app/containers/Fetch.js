import React from 'react';

import '../favicon.ico';
import '../css/boots.scss';
// import '../js/index.js';

import { Guilas } from '../components/Guilas';

class CustomApp extends React.Component {
  constructor() {
    super();
    this.state = { users: [] , isLoading: false , error: null };
  }

  fetchRandomUsers() {

    const baseUrl = 'https://randomuser.me/api/';
    const query = '?format=json&results=25&exc=login&noinfo&gender=female&nat=us,nz,nl,ie,gb,fr,fi,es,dk,de,ch,ca,br,au';

    fetch(`${baseUrl}${query}`)
    .then( (response) => { 
      if(response.ok){ // console.log("response" , response );
        return response.json(); 
      }
      else {
        throw new Error ( 'Something has gone wrong...' );
      }      
    } )
    .then( (data) => { // console.log( "data" , data );
      this.setState( { users: data.results , isLoading: false } );
    } )
    .catch( (error) => { 
      this.setState( { error , isLoading: false  } );
      console.log(error); 
    } );
  }

  componentDidMount() {
    this.setState( {isLoading: true } );
    this.fetchRandomUsers();
  }

  render(){
    return(
      <div className="wrapperino">
        <Guilas state={this.state}/>
      </div>
    );
  };
}

export {CustomApp};