import React from 'react';

const Guilas = ( {state} ) => {

  const {users} = state;
  const error = state.error;
  const isLoading = state.isLoading;

  // console.log( "Guilas component" , {users} ); // object
  // console.log( "Guilas component" , users ); // array
  
  const list = users.map( (user) => {
    let dob = new Date(user.dob);
    let altstring = `picture of ${user.name.first} ${user.name.last}`;
    return(
      <div key={ Date.parse(user.registered ) } className="bordered">
        <div className="imageCon">
          <img src={user.picture.large} alt={altstring} />
        </div>
        <div className="textCon">
          <p>Name: {user.name.title} {user.name.first} {user.name.last}</p>
          <p>DOB: { dob.getMonth() }/{ dob.getDate() }/{ dob.getFullYear() } </p>
          <p>Country \ Nationality: <span className="nationality">{user.location.city}, {user.location.state}</span>, {user.nat}</p>
        </div>
      </div>
    );
  } );

  if(error){
    return(
      <div>
       <p>{error.message}</p>
      </div>
    ); 
  }

  if( isLoading ){
    return(
      <div>
       <p>Loading &hellip;</p>
      </div>
    ); 
  }

  return(
    <div>
      <h1>Wow! Fetch can be such fun!</h1>
      {list}
    </div>
  );
};

export {Guilas};