# Description
Fun with ReactJS, arrays, and the Fetch API.

## Quick word about babel-preset-env
During compilation I was warned about **env** and to visit the [babel homepage](http://babeljs.io/env "link to env advisory") to read further on the subject. Although, I did not utilize **babel-preset-es2015** specifically, it is safe to say that **babel-preset-env** is going to be shelved in favor of **&#64;babel/preset-env**. You can see the changes in the commit history if you desire. Also, you may as well go for the gusto in this case. In the short run, you would run something to this effect to swap out one package for the other like so:
```sh
$ npm uninstall babel-preset-env babel-core babel-loader babel-preset-react --save-dev
$ npm install @babel/preset-env @babel/core "babel-loader@^8.0.0-beta" @babel/preset-react --save-dev
```

## Please visit
You can see [the lesson](https://artavia.gitlab.io/reference_types_and_react/dist/index.html "the lesson at github") and decide if this is something for you to play around with at a later date. 

## Purpose of this toy project
I had been meaning to revisit arrays and the Fetch API. Arrays are important because they can make or break an app. I have published previous works that definitely could have benefitted from arrays. But to keep things real, I prefer to include any and all previous design flaws. This is so that I can demonstrate in linear fashion that as time goes on I am learning from any previous experience. It wouldn&rsquo;t be genuine on my part to tweak a project to the point of being pristine. We&rsquo;re all human after all! And, I am still learning. I had previously been exposed to the Fetch API in the context of some cool cats at a website called [Full Stack React](https://www.fullstackreact.com/react-daily-ui/005-app-icon/ "link to React Daily UI number five"). I did not like some of the internals that dealt with obtaining geolocation based on an IP address. But, I was able to refactor my own copy of their project into one that used a [promisified geolocation response](https://stackoverflow.com/questions/36995628/how-to-implement-promises-with-the-html5-geolocation-api "link to stackoverflow") that seemed to gel very well with the previous code in lieu of their geolocation response based on an IP address.

## Smart versus Dumb Components
I slapped this together after learning the basics ReactJS with Redux and I was reminded of the distinction between Stateful versus Stateless components. Therefore, I will probably be separating the two different species of components from now on.

## Helpful sources
For this project, I searched the search engines for each &quot;reactjs and arrays,&quot; plus, &quot;reactjs ajax fetch API.&quot; That said, I was able to consult a tutorial called [How to Fetch Data in React](https://www.robinwieruch.de/react-fetching-data "link to Fetching Data in React article"). Thank you, Robin! Plus, since I will eventually get around to building an original OpenWeather API app someday, I decided to use another filler data API which led me to a site called [RandomUser](https://randomuser.me/documentation "link to RandomUser API"). Hence, this is what I came up with for my own personal experimentation and learning. 

### My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!